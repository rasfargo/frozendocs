##Drone Code Markdown


####Steps involved
1. Determine if a shot is available based on following variables:
* Position compared to enemy
* Time until impact
2. **Decision** - If shot is available 
* If yes, Fire shot and destroy enemy drone, ***ending program***.
* If no, Move to next path segment and target nearest enemy until one is within the variables to make a shot.